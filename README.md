# Telescope challenge

Write a python script that will access a bunch of documents uploaded in a google drive and 
will enable us to ask natural language queries.

Don’t worry about doing UI stuff, the natural language queries can be simply hardcoded or 
passed as command line arguments.

Also the outputs can be simple print or log statements.
The goal is to take an abstract problem and come up with quick prototypes that can work.

# Steps

You need to have your google developer account and access to gcloud either with your personal account
or with the credentials that your company has provided you or iam account.

For the sake of this challenge, i'm using my personal account.

- Install [pulumi](https://www.pulumi.com/docs/install/)
- Install [gcloud](https://cloud.google.com/sdk/docs/install)
- Install [pyenv](https://github.com/pyenv/pyenv#installation) and [pyenvvirtualenv](https://github.com/pyenv/pyenv-virtualenv#installing-as-a-pyenv-plugin) 
  or whatever you use to handle venvs in python

Authenticate with google cloud

`gcloud auth login`

Go to the infra directory _`cd infra`_ and type gcloud auth, follow the steps and click ok.
type

`pulumi login --local`

`pulumi stack init dev`_You are going to be creating a stack. A stack is basically an environment, this is a challenge, so we will have only one env_

Do not set a passphrase, this is a simple challenge we just want to test this.

create a virtualenvironment. I won't detail this, since I'm guessing you have your own way to do this

`pip install -r requirements.txt`

`pulumi up`

```
It will take a couple of seconds, this will output some values, such as

Outputs:
    project_id           : "query-drive-4e6db339"
    service_account_email: "service-account-drive@query-drive-4e6db339.iam.gserviceaccount.com"
```
__Those are my values, i'm providing them as an example__

Set up gcp sdk to your newly created project

`gcloud config set project query-drive-4e6db339`
`export PULUMI_CONFIG_PASSPHRASE=`
`pulumi stack output sa_key --show-secrets|base64 --decode > service_account.json`

This command simply gets the service account we created for this challenge.
I'm doing this so you don't have to worry about sharing your personal account.

# Step 2

Assuming that everything is created we will proceed to execute the second part.
We will need to populate the gdrive folder with the json file in folder_names.json
Don't worry, these are simple values that if you open the file you can see the structure.
You need to provide the environmental variable for your e-mail, so the folders are shared with you
and you can later on populate with real files and perform the nltk lookup by yourself
without these hardcoded values, since this sort of test-case will prove what happens when you search
and you find something and when you search and don't obtain a response with my values
but in order for you to test this a bit more, you can upload files and perform the lookup

If you checked out this project in the path /home/jvazquez/workspace/telescope-challenge, in that cwd type this
with your venv enabled

`pip install requirements.txt`

 `PYTHONPATH=. MY_EMAIL=jorgeomar.vazquez@gmail.com SERVICE_ACCOUNT_FILE="/home/jvazquez/workspace/telescope-challenge/infra/service_account.json" python app/populate.py`

We provide the email via an env-var, the same for the service account to access the drive
This should had been generated on step 1

# Step 3

Finally, you are running lookups.
With your virtual environment enabled, run

`PYTHONPATH=. SERVICE_ACCOUNT_FILE="/home/jvazquez/workspace/telescope-challenge/infra/service_account.json"  python app/lookups.py`

This will perform a very basic lookup against the drive folders we created

`PYTHONPATH=. SERVICE_ACCOUNT_FILE="/home/jvazquez/workspace/telescope-challenge/infra/service_account.json"  python app/lookups.py cats.jpg`

This will perform a very basic lookup against the drive folders we created with the command line arguments

`PYTHONPATH=. SERVICE_ACCOUNT_FILE="/home/jvazquez/workspace/telescope-challenge/infra/service_account.json" python app/lookups.py --use-nltk "find me a file called dogs.jpg"`

This will perform a very basic lookup against the drive folders we created with the command line arguments
but we will perform a preprocessing with nltk before we call the drive service

`PYTHONPATH=. SERVICE_ACCOUNT_FILE="/home/jvazquez/workspace/telescope-challenge/infra/service_account.json" python app/lookups.py --use-nltk`

This will perform a search strategy using a really simplistic approach to nlp


# Clean up

Ok, time to destroy this

`gcloud projects delete query-drive-4e6db339`

`pulumi stack rm -y --force dev`

This will delete the project in gcloud and remove the pulumi files


# Final questions

2 - Attention to detail! How are you going to present the solution?

I'm recording a [youtube video](https://youtu.be/-LWm4WYarfE) showing this project running, 
from the setup to the execution.

3 - ReadME file - how are you going to present the solution to someone who is remote? How well are you explaining it?

I hope that the video and me reading this readme documents this, doing this kind of setups is what I like the most.

4 - Improvements
- Dockerize it
- Provide a makefile with targets that do everything for the user so he doesn't have to worry about things like pulumi or gcp
- The NLP part is quite simplistic, this can be improved a lot, I'm not even scratching the surface of NLP, but I provide first class functions to simulate a strategy, this can be improved
 with a much more complex functionality, but the idea was to have a simple project working
- Testing, both functional and regular unit tests, but since this something fast, there's not testing. I've not tested basic things like filenames  with spaces  or the populate script with spaces.
-

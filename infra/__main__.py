import uuid
import os

import pulumi
import pulumi_gcp as gcp

config = pulumi.Config()
project_id = config.get('project_id')

if not project_id:
    project_id = f"query-drive-{str(uuid.uuid4())[:8]}"
    # Instead of setting the secret in the config, set it as an environment variable
    os.environ['PULUMI_PROJECT_ID'] = project_id

# Use the project_id to create a GCP project
project = gcp.organizations.Project('telescope-drive-challenge',
                                    name='query-drive',
                                    project_id=project_id)

# Enable the Google Drive API for the project
drive_api = gcp.projects.Service('drive-api',
                                 service='drive.googleapis.com',
                                 project=project.project_id)

service_account = gcp.serviceaccount.Account('drive-executor',
                                             account_id='service-account-drive',  # Must be unique within the project
                                             display_name='My drive executor',
                                             project=project.project_id)

sa_key = gcp.serviceaccount.Key('sa-key',
                                service_account_id=service_account.name,
                                public_key_type='TYPE_X509_PEM_FILE')

# Export the service account email and project ID
pulumi.export('service_account_email', service_account.email)
pulumi.export('project_id', project.project_id)
pulumi.export('sa_key', sa_key.private_key)

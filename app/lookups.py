import sys

import nltk

from google.oauth2 import service_account
from googleapiclient.discovery import build
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize

from app.misc import get_service_account_file

SERVICE_ACCOUNT_FILE = get_service_account_file()
credentials = service_account.Credentials.from_service_account_file(
    SERVICE_ACCOUNT_FILE, scopes=['https://www.googleapis.com/auth/drive'])

# Build the service
service = build('drive', 'v3', credentials=credentials)


def process_query_nltk(query):
    words = word_tokenize(query)
    # Remove stopwords
    words = [word for word in words if word not in stopwords.words('english')]
    # Our search criteria for the sake of brevity will be "find a file called ..."
    if "called" in words:
        name_index = words.index('called') + 1
        if name_index < len(words):
            # Return the file name directly without stemming
            return words[name_index]

    return " ".join(words)


def search_drive(query: str):
    response = service.files().list(q=f"name contains '{query}' and trashed=false",
                                    spaces='drive',
                                    fields='files(id, name, mimeType)').execute()
    files = response.get('files', [])

    # Check if the files list is empty
    if not files:
        print(f"No files found with the name containing '{query}'.")
    else:
        # Output the search results
        for file in files:
            print(f"Found file: {file.get('name')} (ID: {file.get('id')}) - MimeType: {file.get('mimeType')}")


def process_query_simple(query):
    return query


def download_nltk_data():
    # Define the required NLTK datasets to download
    required_datasets = ['punkt', 'stopwords']

    # Loop over each dataset and check if it's present; if not, download it
    for dataset in required_datasets:
        try:
            nltk.data.find(f'tokenizers/{dataset}')
        except LookupError:
            nltk.download(dataset)


if __name__ == "__main__":
    # Call the function at the start of your script
    download_nltk_data()
    use_nltk = '--use-nltk' in sys.argv
    args = [arg for arg in sys.argv[1:] if arg != '--use-nltk']

    # If there are no additional arguments, use the hardcoded list
    if not args:
        if use_nltk:
            stub_terms = ["find me a file called cats.jpg", "find me a file called fake.file",
                          "find me a file called balance.xls"]
            print("I will perform these lookups with our custom predefined search language"
                  f"{stub_terms}")
            for term in stub_terms:
                processed_query = process_query_nltk(term)
                search_drive(processed_query)
        else:
            stub_terms = ["cats.jpg", "fake.file", "balance.xls", "fixed_file.md"]
            for term in stub_terms:
                search_drive(term)
    else:
        query = ' '.join(args)
        if use_nltk:
            processed_query = process_query_nltk(query)
            search_drive(processed_query)
        else:
            search_drive(query)

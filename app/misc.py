import os
import sys


def get_service_account_file():
    service_account_file = os.getenv("SERVICE_ACCOUNT_FILE", None)
    if service_account_file is None:
        print("Env var SERVICE_ACCOUNT_FILE is not set up. "
              "This looks like a path and it is the full path to the service account.json file "
              "that you generated on the step 2 of the readme")
        sys.exit(-1)
    return service_account_file


def get_my_email():
    email = os.getenv("MY_EMAIL", None)
    if email is None:
        print("Env var MY_EMAIL is not set up. "
              "I just need this var so I can share the folders created on step 2 "
              "with your account.")
        sys.exit(-1)
    return email
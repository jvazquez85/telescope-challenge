import json
import time

from os.path import dirname, abspath, join

from google.oauth2 import service_account
from googleapiclient.discovery import build
from googleapiclient.errors import HttpError

from app.misc import get_service_account_file, get_my_email

SERVICE_ACCOUNT_FILE = get_service_account_file()
SAMPLE_STUB_FILE = join(dirname(__file__), abspath(dirname(__file__)), "samples/folder_names.json")
credentials = service_account.Credentials.from_service_account_file(
    SERVICE_ACCOUNT_FILE, scopes=['https://www.googleapis.com/auth/drive'])

service = build('drive', 'v3', credentials=credentials)

with open(SAMPLE_STUB_FILE, 'r') as file:
    data = json.load(file)


def share_with_user(service, folder_id, user_email):
    user_permission = {
        'type': 'user',
        'role': 'writer',  # Use 'writer' to grant edit permissions
        'emailAddress': user_email
    }
    request = service.permissions().create(
        fileId=folder_id,
        body=user_permission,
        fields='id',
        sendNotificationEmail=False,
    )
    response = request.execute()
    print(f"Shared with {user_email}. Permission ID: {response.get('id')}")


def find_folder(name):
    response = service.files().list(q=f"mimeType='application/vnd.google-apps.folder' and name='{name}' and trashed=false",
                                    spaces='drive',
                                    fields='files(id, name)').execute()
    for folder in response.get('files', []):
        if folder.get('name') == name:
            return folder.get('id')
    return None


def exponential_backoff_retry(request_func, *args, **kwargs):
    max_retries = 5
    backoff_factor = 2
    wait_time = 1  # Initial wait time of 1 second

    for i in range(max_retries):
        try:
            return request_func(*args, **kwargs)
        except HttpError as e:
            if e.resp.status in [403, 429]:
                # Check if rate limit related to sharing quota
                if 'sharingRateLimitExceeded' in str(e):
                    print(f"Rate limit exceeded, retrying in {wait_time} seconds...")
                    time.sleep(wait_time)
                    wait_time *= backoff_factor
                else:
                    # If the error is not related to rate limits, re-raise it
                    raise
            else:
                # If the error is not a 403 or 429, re-raise it
                raise
        except Exception as e:
            print(f"An error occurred: {e}")
            raise
    raise Exception(f"Failed to complete the request after {max_retries} retries.")


def create_folder(name: str):
    folder_id = find_folder(name)
    user_email = get_my_email()
    if folder_id is not None:
        print(f"Folder '{name}' already exists.")
    else:
        folder_metadata = {
            'name': name,
            'mimeType': 'application/vnd.google-apps.folder'
        }
        # Wrap the API call with the exponential_backoff_retry function
        folder = exponential_backoff_retry(
            service.files().create(body=folder_metadata, fields='id').execute
        )
        folder_id = folder.get('id')
        print(f"Created folder '{name}' with ID: {folder_id}")

    # Now that we have a valid folder_id, share the folder
    share_with_user(service, folder_id, user_email)
    return folder_id


def find_file(name: str, folder_id: int):
    response = service.files().list(q=f"name='{name}' and '{folder_id}' in parents and trashed=false",
                                    spaces='drive',
                                    fields='files(id, name)').execute()
    for file in response.get('files', []):
        if file.get('name') == name:
            return file.get('id')
    return None


def create_file(file_meta, folder_id):
    file_id = find_file(file_meta['name'], folder_id)
    if file_id is not None:
        print(f"File '{file_meta['name']}' already exists in folder ID {folder_id}.")
        return file_id

    file_metadata = {
        'name': file_meta['name'],
        'parents': [folder_id],
        'mimeType': 'application/vnd.google-apps.document'
    }
    file = service.files().create(body=file_metadata, fields='id').execute()
    return file.get('id')


folder_ids = {folder_name: create_folder(folder_name) for folder_name in data['folders']}

for file in data['files']:
    folder_id = folder_ids[file['folder']]
    file_id = create_file(file, folder_id)
    print(f"Created file {file['name']} in folder {file['folder']} with ID: {file_id}")
